<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Weatherresource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'cityname' => $this->cityname,
            'date' => $this->date,
            'temp_min' => $this->temp_min,
            'temp_max' => $this->temp_max,
            'pressure' => $this->pressure,
            'sea_level' => $this->sea_level,
            'grnd_level' => $this->grnd_level,
            'humidity' => $this->humidity,
            'wind_speed' => $this->wind_speed,
            'wind_deg' => $this->wind_deg
        ];
    }
}
