<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Cmfcmf\OpenWeatherMap;
use Http\Factory\Guzzle\RequestFactory;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;

use App\Weather;
use Log;

class WeatherOfWeek extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'weather:week';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Busca el clima de 5 dias cada 3 horas y lo persiste en una base de datos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $lang = 'es';
        $units = 'metric';
        $myApiKey = 'ef5da1d085617b3288f469cd88890ae4';

        $httpRequestFactory = new RequestFactory();
        $httpClient = GuzzleAdapter::createWithConfig([]);

        $owm = new OpenWeatherMap($myApiKey, $httpClient, $httpRequestFactory);
        // Example 1: Get forecast for the next 5 days for Berlin.
        $forecast = $owm->getWeatherForecast('Puerto Madryn', $units, $lang, '', 1);

        foreach ($forecast as $weather) {
            //echo dd($weather->wind->speed);
            $weather = Weather::create(['cityname' => $weather->city->name, 'date' => $weather->time->from, 'temp_min' => $weather->temperature->min, 'temp_max' => $weather->temperature->max, 'pressure' => $weather->pressure, 'humidity' => $weather->humidity, 'wind_speed' => $weather->wind->speed]);
            //Log::info($weather);
            //echo $weather->temperature;
        }
    }
}
